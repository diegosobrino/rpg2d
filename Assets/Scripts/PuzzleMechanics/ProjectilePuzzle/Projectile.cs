using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [Tooltip("Speed of the projectile")]
    public float speed = 4;

    [Tooltip("Time that the projectile takes to dissapear"), SerializeField]
    float activeTime = 3;

    private void OnEnable()
    {
        Destroy(gameObject, activeTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.GetComponent<Player>())
            Destroy(gameObject);
    }
}
