using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField, Tooltip("Explosion prefab")]
    GameObject explosion;

    [SerializeField, Tooltip("Time it takes to explode")]
    float expTime;

    private void OnEnable()
    {
        //Hotfix
        transform.position += Vector3.up * 0.1f + Vector3.right * 0.05f;

        Invoke("Explosion", expTime);
        Destroy(gameObject, expTime);
    }

    void Explosion() => Instantiate(explosion, transform.position, Quaternion.identity);
}
