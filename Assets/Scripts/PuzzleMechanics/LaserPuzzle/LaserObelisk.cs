using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserObelisk : MonoBehaviour
{
    [Tooltip("Max length of the ray"), SerializeField]
    float length;

    [Tooltip("Width of the ray"), SerializeField]
    float width;

    int rayPointsN;

    [Tooltip("The transform which the laser uses to orientate"), SerializeField]
    Transform rayCaster;

    LineRenderer lr;

    Ray2D ray;
    RaycastHit2D hit;
    Vector3 refDirection;
    LaserTarget lt;
    bool targetHit;

    private void Awake()
    {
        lr = GetComponent<LineRenderer>();

        lr.startWidth = width;
        lr.endWidth = width;
    }

    private void FixedUpdate()
    {
        Laser();
    }

    void Laser()
    {
        rayPointsN = 1;

        ray = new Ray2D(rayCaster.position, rayCaster.up);

        lr.positionCount = rayPointsN;

        lr.SetPosition(0, rayCaster.position);

        for (int i = 0; i <= 4; i++)
        {
            if (i == 0)
            {
                if (Physics2D.Raycast(ray.origin, ray.direction, length))
                {
                    hit = Physics2D.Raycast(ray.origin, ray.direction, length);

                    CheckRayhit();

                    if (hit.transform.gameObject.layer == 6)
                    {
                        refDirection = Vector3.Reflect(ray.direction, hit.normal);

                        ray = new Ray2D(hit.point, refDirection);

                        lr.positionCount = ++rayPointsN;

                        lr.SetPosition(i + 1, hit.point);
                    }

                    else
                    {
                        lr.positionCount = ++rayPointsN;

                        lr.SetPosition(i + 1, hit.point);
                    }
                }

                else
                {
                    lr.positionCount = ++rayPointsN;

                    lr.SetPosition(i + 1, ray.origin + ray.direction * length);
                }
            }

            else
            {
                if (Physics2D.Raycast(hit.point, refDirection, length))
                {
                    hit = Physics2D.Raycast(ray.origin, ray.direction, length);

                    CheckRayhit();

                    if (hit.transform.gameObject.layer == 6)
                    {
                        refDirection = Vector3.Reflect(refDirection, hit.normal);

                        ray = new Ray2D(hit.point, refDirection);

                        lr.positionCount = ++rayPointsN;

                        lr.SetPosition(i + 1, hit.point);
                    }
                }

                else
                {
                    lr.positionCount = ++rayPointsN;

                    lr.SetPosition(lr.positionCount - 1, ray.origin + ray.direction * length);
                }
            }
        }
    }

    void CheckRayhit()
    {
        if (!targetHit && hit.transform.TryGetComponent<LaserTarget>(out lt))
        {
            lt.raycastEvent.Invoke();
            targetHit = true;
        }
    }
}