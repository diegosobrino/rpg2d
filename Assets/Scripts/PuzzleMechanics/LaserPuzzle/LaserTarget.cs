using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LaserTarget : MonoBehaviour
{
    [Header("Action/s that will be executed when the laser touches this target")]
    public UnityEvent raycastEvent;
}
