using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Tooltip("Pefab of the projectile"), SerializeField]
    GameObject projectile;

    [Tooltip("Pefab of the bomb"), SerializeField]
    GameObject bomb;

    [Tooltip("Time between shots"), SerializeField]
    float shootCD = 1;

    [Tooltip("Cooldown for planting a bomb"), SerializeField]
    float bombCD = 1;

    float shootTimer, bombTimer;

    Rigidbody2D rb;

    bool shootOnCD, bombOnCD;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        shootTimer = shootCD;
    }

    private void Update()
    {
        Shoot();
        Bomb();
    }

    void Shoot()
    {
        if (shootOnCD && shootTimer > 0)
        {
            shootTimer -= Time.deltaTime;
            return;
        }

        if (Input.GetKeyDown("z"))
        {
            shootOnCD = true;
            shootTimer = shootCD;
            Rigidbody2D rbp = Instantiate(projectile, transform.position, Quaternion.identity).GetComponent<Rigidbody2D>();
            rbp.velocity = rb.velocity.normalized * rbp.gameObject.GetComponent<Projectile>().speed;
        }
    }

    void Bomb()
    {
        if (bombOnCD && bombTimer > 0)
        {
            bombTimer -= Time.deltaTime;
            return;
        }

        if (Input.GetKeyDown("x"))
        {
            bombOnCD = true;
            bombTimer = bombCD;
            Instantiate(bomb, transform.position, Quaternion.identity);
        }
    }
}
