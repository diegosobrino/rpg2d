using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour
{  
    [Header("Quest Log")]
    public List<Quest1> questList = new List<Quest1>();

    public List<Quest1> activeQuest = new List<Quest1> ();
    public List<Quest1> standByQuest = new List<Quest1> ();


    private void Awake()
    {
        foreach (Quest1 quest in GetComponentsInChildren<Quest1>())
        {
            questList.Add(quest);
        }
    }

    /// <summary>
    /// When a quest must perform in the world it may need to abort or standby others events in the world
    /// </summary>
    /// <param name="soloquest"></param>
    public void SoloQuest(Quest1 soloquest)
    {
        foreach (Quest1 quest in activeQuest)
        {
            if (quest == soloquest)
            {
                continue;
            }

            activeQuest.Remove(quest);
            standByQuest.Add(quest);
            //abortar
            quest.StandByQuest();
        }
    }

    /// <summary>
    /// This funtion is called to resume all quest that were in Stand By by other events
    /// </summary>
    public void ResumeQuest()
    {
        foreach (Quest1 quest in standByQuest)
        {
            quest.ActiveQuest();
           
        }
        standByQuest.Clear();
    }




}
