using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestObjectiveTarget : QuestObjetive
{
    Quest1 quest;


    [Header("Object Target")]
    public TargetQuest targetQuest;

    [Tooltip("Number of targets that have to complete")]
    public int numberTargets;
    int currentTargets;
    private void Awake()
    {
        quest = GetComponentInParent<Quest1>();
    }

    public void TargetSucces()
    {
        currentTargets++;
        UpdateConditions();

    }

    void UpdateConditions()
    {
        Debug.Log(currentTargets + " / " + currentTargets);

        if (currentTargets >= numberTargets)
        {
            Debug.Log("ObjetiveCompleted");
            Status = ObjetiveStatus.Completed;
            quest.UpdateObjetiveList(this, Status);
        }

    }

}
