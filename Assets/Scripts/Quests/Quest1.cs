using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest1 : MonoBehaviour
{
    QuestManager questManager;
    public string nameQuest;

    private void Awake()
    {
        questManager = GetComponentInParent<QuestManager>();

        foreach (QuestObjetive objetive in GetComponentsInChildren<QuestObjetive>())
        {
            questObjetivesList.Add(objetive);
        }
        foreach (Reward reward in GetComponentsInChildren<Reward>())
        {
            rewards.Add(reward);
        }
    }

    public enum QuestStatus
    {
        inactive,           // Not avaible for the player
        avaible,            // Avaible for the player to start
        inprogress,         // Player started the mision
        completed,          // Player has reached the condition of the quest
        finished            // Has conclude the mision with the rewards
    }

    [Header("Begin")]
    public BeginQuest beginMode;
    public enum BeginQuest
    {
        trigger,
        questGiver,
        sequencial
    }



    [Header("Objectives")]
    [Tooltip("List of objectives to achieve quest")]
    [SerializeField] List<QuestObjetive> questObjetivesList = new List<QuestObjetive>();
    [SerializeField] bool questCompleted;
    [SerializeField] bool notCompleted;

    [Tooltip("If the quest interrupt other quests/events")]
    public bool soloQuest;
    public QuestStatus currentStatus;

    [Tooltip("Does the quest still avaible if you decline?")]
    public bool persistent;

    [Header("Progression")]
    [SerializeField] bool hasReward;
    [SerializeField] bool selection;
    [Tooltip("Experience that you gain when the quest is finished")]
    public int expReward = 100;
    [Header("Rewards ")]
    List<Reward> rewards = new List<Reward>();
    public void UpdateObjetiveList(QuestObjetive completedObjective, QuestObjetive.ObjetiveStatus status)
    {
        foreach (QuestObjetive objetive in questObjetivesList)
        {
            if (objetive.Status != QuestObjetive.ObjetiveStatus.Completed)
            {
                notCompleted = true;
                continue;
            }
            if (objetive == completedObjective)
            {

                if (status == QuestObjetive.ObjetiveStatus.Completed)
                {
                 // Update UI Quest Log   
                 // Update Objetives UI table: new target
                 // Update Dialogue Event



                }
            }
        }

        if (notCompleted == true)
        {
            return;
        }

        Debug.Log("Quest Completed!!");
        CompleteQuest();

    }

  

    void ChangeStatus(QuestStatus newStatus)
    {
        currentStatus = newStatus;
    }

    /// <summary>
    /// Active the Quest so the player can start 
    /// </summary>
    public void ActiveQuest()
    {
        ChangeStatus(QuestStatus.avaible);
    }

    /// <summary>
    ///  You make the mission to never start
    /// </summary>
    public void DisableQuest()
    {
        persistent = false;
        ChangeStatus(QuestStatus.inactive);
    }

    /// <summary>
    ///  If you start a mision or is there an event the avaible mission must disable
    /// </summary>
    public void StandByQuest()
    {
        ChangeStatus(QuestStatus.inactive);
    }



    /// <summary>
    /// This funtion is called for beign the mission
    /// </summary>
    public void StartQuest()
    {
        if (soloQuest)
        {
            questManager.SoloQuest(this);
        }


        SetObjetives();
    }

    public void SetObjetives()
    {
        foreach (QuestObjetive obj in questObjetivesList)
        {
            obj.Status = QuestObjetive.ObjetiveStatus.Active;
        }

    }

    public void CompleteQuest()
    {
        ChangeStatus(QuestStatus.completed);
    }
    /// <summary>
    /// This is called when the mission is completed and you speak with the NPC that will trigger this event and give you the reward;
    /// </summary>
    void FinishQuest()
    {

        if (hasReward)
        {
            OfferReward();
        }
    }

    /// <summary>
    /// NPC Offer the reward to the player: Can be multichoice or just the reward. Either ways player must interact to recieve the reward
    /// </summary>
    void OfferReward()
    {
        
        if (selection)
        {
            // Se le muestran las opciones
            ChooseReward();
        }
        else
        {
            // Se le muestran las recompensas
            RecieveReward();
        }
    }

    /// <summary>
    /// Player can choose between different options
    /// </summary>
    void ChooseReward()
    {
        bool playerDecision = true;

        if (playerDecision)
        {
            //Reward A
        }
        else
        {
           //Reward B
        }


    }

    void RecieveReward()
    {
        // Presh button to recieve reward

    }

}
