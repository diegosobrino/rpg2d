using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetQuest : MonoBehaviour
{

    [SerializeField] QuestObjectiveTarget objectiveTarget;



    private void Awake()
    {
     
        //Suscription to onDeathUnit
   
    }
    void OnCompleted()
    {
        if (objectiveTarget == null)
        {
            Debug.LogError("ObjetiveTarget no asignado");
            return;
        }
        objectiveTarget.TargetSucces();
    }


}
