using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestGiver : MonoBehaviour
{
    [SerializeField] bool isAvaible;


    public Quest1 quest;
    bool playerDecision;

    private void Awake()
    {
        if (quest == null)
        {
            Debug.LogWarning("Quest is not assigned");
        }
    }

    /// <summary>
    /// This is called when player interact with the NPC
    /// </summary>
    void OfferQuest()
    {
        if (playerDecision)
        {
            StartQuest();
        }
        else
        {
            DeclineQuest();
        }
    }

    void StartQuest()
    {
        quest.StartQuest();
    }    
    void DeclineQuest()
    {
        if (!quest.persistent)
        {
            isAvaible = false;
            quest.DisableQuest();
        }
    }
}
